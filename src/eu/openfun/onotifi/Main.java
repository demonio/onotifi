package eu.openfun.onotifi;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import net.canarymod.Canary;
import net.canarymod.plugin.Plugin;
import net.canarymod.plugin.PluginListener;

public class Main extends Plugin { 

    private final PluginListener MainListener = new MainListener();

	@Override
    public boolean enable() {
		Config config = new Config();
		config.read();
		if(config.password == "password"){
			getLogman().info("Configure this plugin first at config/MCTown/config.txt");
			return false;
		}else {
			Statement stmt = null;
			Connection connection = null;
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			try {
				connection = DriverManager
				.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
		 
			} catch (SQLException e) {
				getLogman().info("Database not found");
				getLogman().info("jdbc:mysql://"+config.server+":3306/"+config.db+" - "+config.username+" - "+config.password);
				e.printStackTrace();
				return false;
			}
			String query =  "CREATE TABLE IF NOT EXISTS `users` (";
			query += "`id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,";
			query += "`name` char(64) NOT NULL,";
			query += "`date` char(64) NOT NULL";
			query += ");";
			
			try {
				stmt = connection.createStatement();
				stmt.execute(query);
				stmt.close();
				getLogman().info("Database initialized");
			} catch (SQLException e) {
				e.printStackTrace();
				getLogman().info("Database Error");
			}
		}
		
		File theDir = new File("databases/MCSkills");
		if (!theDir.exists()) theDir.mkdir();
        Canary.hooks().registerListener(MainListener, this);
        getLogman().info("Enabling "+getName() + " Version " + getVersion());
        getLogman().info("Authored by "+getAuthor());
        return true;
    }

	@Override 
    public void disable() {   
        Canary.hooks().unregisterPluginListener(MainListener);
        getLogman().info("Disabling "+getName());
    } 
}
