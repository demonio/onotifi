package eu.openfun.onotifi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.canarymod.hook.HookHandler;
import net.canarymod.hook.player.ConnectionHook;
import net.canarymod.plugin.PluginListener;

public class MainListener implements PluginListener {

	@HookHandler
	public void user_login(ConnectionHook hook){
		Config config = new Config();
		config.read();
		String name = hook.getPlayer().getName();
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String time = dateFormat.format(date);
		
		String query = "INSERT INTO users (name, date) VALUES ('"+name+"', '"+time+"')";
		Statement stmt = null;
		Connection connection = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		try {
			connection = DriverManager
			.getConnection("jdbc:mysql://"+config.server+":3306/"+config.db,config.username,config.password);
	 
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			stmt = connection.createStatement();
			stmt.execute(query);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		hook.getPlayer().message("If there is no Admin online one will come here soon :-)");
	}
	
}
